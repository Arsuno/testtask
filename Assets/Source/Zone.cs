﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

using Random = System.Random;

public class Zone : MonoBehaviour, IPointerClickHandler
{
    public event Action OutOfObjects;
    [SerializeField] private ZoneSettings _zoneSettings;

    private readonly List<ZoneObject> _availableObjects = new List<ZoneObject>();
    private readonly List<ZoneObject> _closedObjects = new List<ZoneObject>();

    private class ZoneObject
    {
        public GameObject Prefab;
        public int CurrentAmount;
        public int MaxAmount;

        public bool IsClosed => CurrentAmount >= MaxAmount;
    }

    private void Awake()
    {
        IEnumerable<ZoneObjectSettings> objectsSettings = _zoneSettings.ObjectsSettings;

        foreach (ZoneObjectSettings objectSetting in objectsSettings)
        {
            _availableObjects.Add(new ZoneObject()
            {
                MaxAmount = objectSetting.Amount,
                CurrentAmount = 0,
                Prefab = objectSetting.Prefab,
            });
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (_availableObjects.Count <= 0)
            return;

        Random random = new Random();
         
        int index = random.Next(0, _availableObjects.Count);

        ZoneObject availableObject = _availableObjects[index];
        availableObject.CurrentAmount++;

        GameObject spawnedObject = Instantiate(availableObject.Prefab, transform);
        spawnedObject.transform.position = Input.mousePosition;


        if (availableObject.IsClosed == true)
        {
            _availableObjects.Remove(availableObject);
            _closedObjects.Add(availableObject);

            if (_availableObjects.Count <= 0)
                OutOfObjects?.Invoke();
        }
    }
}
