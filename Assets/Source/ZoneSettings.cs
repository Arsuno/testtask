using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ZoneSettings", menuName = "ZoneSettings")]
public class ZoneSettings : ScriptableObject
{
    [SerializeField] private string _zoneName;
    [SerializeField] private ZoneObjectSettings[] _objects;

    public string ZoneName => _zoneName;
    public IEnumerable<ZoneObjectSettings> ObjectsSettings => _objects;

}

[Serializable]
public class ZoneObjectSettings
{
    [SerializeField] private GameObject _prefab;
    [SerializeField] private int _amount;
    public int Amount => _amount;
    public GameObject Prefab => _prefab;
}