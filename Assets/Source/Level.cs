﻿using System;
using UnityEngine;

public class Level : MonoBehaviour
{
    public event Action OutOfFields;

    [SerializeField] private Zone[] _zones;

    private int _amountOfFilledFields;

    private void OnEnable()
    {
        foreach (Zone zone in _zones)
        {
            zone.OutOfObjects += OnZoneOutOfObjects;
        }
    }

    private void OnDisable()
    {
        foreach (Zone zone in _zones)
        {
            zone.OutOfObjects -= OnZoneOutOfObjects;
        }
    }

    private void OnZoneOutOfObjects()
    {
        _amountOfFilledFields++;
        Debug.Log(_amountOfFilledFields);

        if (_amountOfFilledFields == _zones.Length)
        {
            OutOfFields?.Invoke();
        }
    }
}