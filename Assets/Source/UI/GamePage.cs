﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GamePage : MonoBehaviour
{
    [SerializeField] private Level _level;
    [SerializeField] private Button _menuButton;

    private const string MenuSceneName = "Menu";

    private void OnEnable()
    {
        _level.OutOfFields += EnableMenuButton;
        _menuButton.onClick.AddListener(OnMenuButtonClicked);
    }

    private void OnDisable()
    {
        _level.OutOfFields -= EnableMenuButton;
        _menuButton.onClick.RemoveListener(OnMenuButtonClicked);
    }

    private void OnMenuButtonClicked()
    {
        SceneManager.LoadScene(MenuSceneName);
    }

    private void EnableMenuButton()
    {
        _menuButton.gameObject.SetActive(true);
    }
}